# typo3camp.ch Website

## Commands

```
docker run --rm --publish 1313:1313 --volume $(pwd):/src registry.gitlab.com/pages/hugo/hugo_extended:latest hugo server --bind 0.0.0.0 --disableFastRender --logLevel debug
xdg-open http://localhost:1313/
```
