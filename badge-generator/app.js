let hasBeenInitialized = false;

function init() {
    applyLocallyStoredSettings();
    hasBeenInitialized = true;
}
function applyLocallyStoredSettings() {
    if (localStorage.getItem('sponsor-data')) {
        document.getElementsByName('sponsor-data')[0].value = localStorage.getItem('sponsor-data');
    }
    if (localStorage.getItem('people-data')) {
        document.getElementsByName('people-data')[0].value = localStorage.getItem('people-data');
    }
    if (localStorage.getItem('card-content-template')) {
        document.querySelector('.card.template .content').innerHTML = localStorage.getItem('card-content-template');
    }
    updateSponsorsOnCardTemplate();
    renderCards();
}

function clearLocallyStoredSettings() {
    localStorage.removeItem('people-data');
    localStorage.removeItem('sponsor-data');
    localStorage.removeItem('card-content-template');
    location.href = location.href;
}

function handlePeopleDataUpdate() {
    if (!hasBeenInitialized) {
        return;
    }
    renderCards();
    localStorage.setItem('people-data', document.getElementsByName('people-data')[0].value);
}

function handleSponsorListUpdate() {
    if (!hasBeenInitialized) {
        return;
    }
    updateSponsorsOnCardTemplate();
    renderCards();
    localStorage.setItem('sponsor-data', document.getElementsByName('sponsor-data')[0].value);
}

function handleCardContentTemplateUpdate() {
    if (!hasBeenInitialized) {
        return;
    }
    renderCards();
    localStorage.setItem('card-content-template', document.querySelector('.card.template .content').innerHTML);
}

function updateSponsorsOnCardTemplate() {
    let cardTemplateSponsors = document.querySelector('.card.template .sponsors');
    cardTemplateSponsors.innerHTML = '';
    let sponsorIds = document.getElementsByName('sponsor-data')[0].value.split(' ');
    sponsorIds.forEach(sponsorId => {
        if (!sponsorId.trim().length) { return; }
        cardTemplateSponsors.innerHTML += `<img src="./../assets/svg/sponsors/${sponsorId}.svg" />`;
    });
}

function renderCards() {
    let pagesRootElement = document.querySelector('.pages');
    pagesRootElement.innerHTML = '';

    let peopleData = document.getElementsByName('people-data')[0].value;
    let columnSeparator = ';';
    if (peopleData.split(',').length > peopleData.split(';').length) {
        columnSeparator = ',';
    }
    if (peopleData.split('\t').length > peopleData.split(',').length) {
        columnSeparator = '\t';
    }
    let peopleDataLines = peopleData.split('\n');

    // Parse template
    let cardTemplate = document.querySelector('.card.template');
    let cardTemplateClone = document.createElement('div');
    cardTemplateClone.classList.add('card');
    cardTemplateClone.innerHTML = cardTemplate.innerHTML;
    let numberOfCardsPerPage = parseInt(pagesRootElement.dataset.cardsPerPage);
    let currentPage = document.createElement('div');

    peopleDataLines.forEach(peopleDataLine => {
        if (peopleDataLine.trim().length === 0) {
            return;
        }
        let card = cardTemplateClone.outerHTML;
        // Replace placeholders (column numbers {0}-{n}
        peopleDataLine.split(columnSeparator).forEach((columnContent, columnIndex) => {
            columnContent = columnContent.trim();
            if (columnContent.match(/^("|').+("|')$/)) {
                columnContent = columnContent.substring(1, columnContent.length-1);
            }
            card = card.replaceAll(`{${columnIndex}}`, columnContent);
        });
        // Replace leftover placeholders
        card = card.replaceAll(/\{[0-9]+}/g, '');

        let cardSet = document.createElement('div');
        cardSet.classList.add('card-set');
        cardSet.innerHTML = `<div>${card}</div><div>${card}</div>`;

        currentPage.innerHTML += cardSet.outerHTML;
        if (numberOfCardsPerPage <= currentPage.children.length) {
            pagesRootElement.innerHTML += currentPage.outerHTML;
            currentPage.innerHTML = '';
        }
    });
    // Add the last generated page to the pages list
    if (currentPage.children.length > 0) {
        pagesRootElement.innerHTML += currentPage.outerHTML;
    }
}