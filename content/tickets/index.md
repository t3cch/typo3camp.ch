---
title: "Tickets"
weight: 55
date: 2024-11-01T12:00:00+01:00
draft: false
menu: main
---


## ~~Early-Bird-Tickets~~

~~Für die Schnellentschlossenen stehen bis zum 31.12.2024 eine limitierte Anzahl an Early-Bird-Tickets, zum Preis von CHF 75.00, zur Verfügung.~~

[Zum Ticketshop](https://eventfrog.ch/t3cch-2025)


## Regular-Tickets

Das reguläre 2-Tages-Ticket gilt für beide Camp-Tage Freitag und Samstag. Darin enthalten sind Verpflegung, Getränke und ein Goodie. Der Preis für ein Regular-Ticket beträgt CHF 85.00.

[Zum Ticketshop](https://eventfrog.ch/t3cch-2025)


## Supporter-Tickets

Mit dem Kauf des Supporter-Tickets, für CHF 110.00, hast du die Möglichkeit, das Camp durch ein persönliches Sponsoring zu unterstützen. Das Ticket gilt für beide Camp-Tage Freitag und Samstag. Darin enthalten sind Verpflegung, Getränke und ein Goodie.

[Zum Ticketshop](https://eventfrog.ch/t3cch-2025)
