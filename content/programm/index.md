---
title: "Programm"
weight: 50
draft: false
menu: main
---

## Donnerstag, 01.05.2025 (ab 19:00)

Am Warm-Up hast du die Gelegenheit, Teilnehmende schon vorab kennenzulernen und/oder mit alten Bekannten ein Bierchen zu trinken. Wir freuen uns auf altbekannte und neue Gesichter. Komm vorbei - erst recht, wenn du alleine unterwegs bist - bisher wurde noch niemand gebissen.

Wir treffen uns im Büro der [Ops One AG](https://opsone.ch/) an der [Weststrasse 77, 8003 Zürich](https://maps.app.goo.gl/RBprTT1j4vkdZu2p9).

## Freitag, 02.05.2025

| Zeit          | Beschreibung                                           |
|---------------|--------------------------------------------------------|
| ab 09.00      | Einlass, Registrierung, Kaffee und Gipfeli             |
| 10:00 - 11:30 | Begrüssung und Sessionplanung                          |
| 11:30 - 12:30 | Sessions                                               |
| 12:30 - 13:30 | Mittagspause                                           |
| 13:30 - 16:30 | Sessions & [TYPO3 Zertifizierung](https://shop.typo3.com/TYPO3-Certification-TYPO3-Camp-Swiss-2025/TY10173.4) |
| 16:30 - 16:45 | Tagesabschluss / Infos Social Event                    |
| ab 17:00      | Social Event                                           |

## Samstag, 03.05.2025

| Zeit          | Beschreibung              |
|---------------|---------------------------|
| ab 09:00      | Kaffee und Gipfeli        |
| 10:00 - 10:30 | Sessionplanung            |
| 10:30 - 12:30 | Sessions                  |
| 12:30 - 13:30 | Mittagspause              |
| 13:30 - 15:30 | Sessions                  |
| 15:30         | Offizielle Verabschiedung |
