---
title: "Location"
weight: 60
date: 2023-12-03T12:00:00+01:00
draft: false
menu: main
toc: true
aliases:
- /unterkunft
---

Das TYPO3camp Schweiz geht in die sechte Runde. 2025 verschlägt es uns wieder nach Zürich! 
Wir freuen uns, zwei spannende Tage in den Räumen der Jugendherberge mit euch zu gestalten und durchzuführen.

![Jugendherberge Zürich - Innenhof](jugi-innenhof.jpg)

## Camp Location

### Anreise
Jugendherberge Zürich \
Mutschellenstrasse 114 \
8038 Zürich

Die Jugi auf [youthhostel.ch](https://www.youthhostel.ch/de/hostels/zuerich) \
Die Jugi auf [Google Maps](https://maps.app.goo.gl/ZVWmQzaiDjq4zz8BA)


#### Öffentliche Verkehrsmittel

Die Jugendherberge ist nur 2 S-Bahn Stationen und ein paar Minuten zu Fuss vom Zürich HB entfernt (via "Zürich Brunau"). Die schnellste Verbindung ab eurem Standort (auch international) findet ihr im [SBB Fahrplan](https://www.sbb.ch/de/kaufen/pages/fahrplan/fahrplan.xhtml?nach=Z%C3%BCrich,%20Jugendherberge&datum=01.05.2025&zeit=17:00&an=true).

#### Auto

Grundsätzlich empfehlen wir, mit den öffentliche Verkehrsmitteln anzureisen. Wer doch gerne motorisiert anreisen möchte, verlässt die Autobahn A3 am besten bei der Ausfahrt 33 "Wollishofen". Von da sind es noch wenige Minuten bis zur Jugi. Achtung: Genügend Zeit einberechnen. Der Verkehr rund um Zürich ist oft eher stockend unterwegs.

### Unterkünfte

#### Jugendherberge :)

* Vom Einzelzimmer mit Dusche/Toilette bis zum Mehrbettzimmer mit Dusche/WC auf dem Flur
* 30 Sekunden bis zum Hauptraum des Camps ;)

https://www.youthhostel.ch/de/hostels/zuerich

#### Das Nächstgelegene: Residence Zürich

* Einzel- & Doppelzimmer
* 750m/10min zur Jugendherberge

https://www.residence-mutschellen.com/
