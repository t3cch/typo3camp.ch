---
title: "Format"
weight: 50
date: 2022-11-07T11:47:00+01:00
draft: false
menu: main
aliases:
- /format
---

Du als Teilnehmende Person bestimmst den Inhalt und den Tagesablauf des Camps
und kannst dieses so [aktiv mitgestalten](/format). Im Vorfeld stellen wir euch
die Möglichkeit zur Verfügung, Sessionvoschläge- und wünsche einzureichen.

## Sessionwunsch oder -Angebot einreichen

Sende uns dein Session *Angebot* oder deinen Session *Wunsch* mit
deinem Namen [via Mail](mailto:info@typo3camp.ch), [twitter (@t3cch)](//twitter.com/t3cch),
oder [Slack (#t3cch)](https://typo3.slack.com/archives/CAVA1P2P7).
w
## Was ist ein TYPO3camp?
Ein TYPO3camp ist nichts anderes als ein [Barcamp](https://de.wikipedia.org/wiki/Barcamp), dessen
Themen sich unter anderem um [TYPO3](//typo3.org) drehen.

### Angebot
Die Teilnehmenden kommen mit ihrem ganzen Wissens- und Erfahrungsschatz an das Camp. Dieses dürfen
sie am Planning dem Plenum anbieten. Das kann dann z.B. so klingen: "Ich könnte zeigen, wie
wir Problem X bei uns gelöst haben und dann könnten wir schauen, wir ihr das für euch
nutzen könntet."

### Nachfrage
Zur selben Zeit, wie Wissen/Austausch *geboten* wird, darf auch explizit *danach gefragt* werden.
"Hey, wir sind unzufrieden mit Tool *XY*, kennt jemand Alternativen? Wollen wir uns dazu
mal austauschen?"

### "Talk" vs "Session"
Wenn Du was anbietest, musst Du nicht unbedingt besser Bescheid wissen, als alle
die an der Session teilnehmen werden (das gäbe ein ziemlich langes Planning).
Eine Session ist ein Wissensaustausch, keine Prüfung.
Teilnehmende können im besten Fall nämlich auch was zu den
besuchten Sessions beitragen. Das ist das, was das Barcamp Format interessant macht.

## Themen
Wir kriegen nicht selten die Rückmeldung, dass zu wenige Sessions zu einem bestimmten
Thema gehalten wurden. Im Nachhinein stellt sich heraus, dass das Thema im Planning gar
nicht *gewünscht* wurde. Nur durch Deinen Wunsch kannst Du wirklich sicherstellen, dass sich
andere melden, die was dazu beizutragen haben. Dabei muss Dein Wunsch noch nicht mal
unendlich konkret sein. Wirf einfach was in die Runde, es sitzt ziemlich sicher irgendwer
mit einem ähnlichen Wunsch im Raum. Und falls nicht genügend Leute für eine "offizielle"
Session zusammenkommen, wäre es nicht das erste Mal, dass die Diskussion plötzlich am
Social Event aufflammt.

Beispiele für "Evergreens" - Themen, für welche oft Sessions zusammenkommen:
Container, Deployment-Strategien, bestimmte TYPO3-Extensions, Erfahrungen mit der
neuesten non-LTS TYPO3-Version, ...

Beispiele für Themen, welche selten abgedeckt werden (aber sehr willkommen wären):
Sämtliche Frontend-Dinge, Challenges in Projektierung/Budgetierung und Projektleitung,
"die 5 schlechtesten Ideen die wir umgesetzt haben", "Komm, wir hacken kurz was zusammen" ...