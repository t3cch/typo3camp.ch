---
title: "Home"
date: 2022-11-07T11:47:00+01:00
draft: false
bodyClasses: transparent-page-header
---
{{< splash title="TYPO3camp Schweiz" subtitle="01. - 03. Mai 2025 - Jugendherberge Zürich" image="images/jugi-zh-aufenthaltsraum.jpg" >}}

{{< panel "Willkommen zum TYPO3camp Schweiz" >}}

Mit dem Gedanken, die Marke TYPO3 in der Schweiz weiter zu stärken, möchten wir vom
TYPO3-Entwickler/Integrator über Projektleiter und Entscheidungsträger bis hin zu TYPO3-Interessierten
alle Interessenbereiche aus der Community zusammen bringen. Sei dabei und teile deine Erfahrungen und
dein Wissen rundum TYPO3.

{{< /panel >}}

# News

{{< newsy-block dateline="Donnerstag, 20. Februar 2024" title="Offizielle TYPO3 Zertifizierung vor Ort möglich" >}}
Auch dieses Jahr habt ihr wieder die Möglichkeit, offizielle TYPO3 Zertifizierungen vor Ort abzulegen. Die Tests finden am **Freitag, 02.05. um 13:30** statt. Bitte melde dich direkt über den [TYPO3 Shop](https://shop.typo3.com/TYPO3-Certification-TYPO3-Camp-Swiss-2025/TY10173.4)
für eine Zertifizierung an.
{{< /newsy-block >}}

{{< newsy-block dateline="Sonntag, 01. Dezember 2024" title=" Vorverkauf TYPO3camp 2025" >}}
Ab sofort sind die Tickets fürs TYPO3camp 2025 verfügbar. Bis Ende Dezember kannst du dir wieder eine limitierte Anzahl Early Bird Tickets sichern. [Alle Infos zu den Tickets gibt's hier](/tickets).
{{< /newsy-block >}}

{{< newsy-block dateline="Montag, 29. Juni 2024" title="Vorankündigung TYPO3camp 2025" >}}
Gestern haben sich die Camporganisatoren zum gemütlichen Beisammensein & Pläne schmieden getroffen.
Es freut uns, bekannt zu geben, dass das TYPO3camp 2025 vom 01. bis 03. Mai wiederum in der
Jugendherberge Zürich stattfinden wird.

Weitere Informationen folgen, nachdem wir den Sommer genossen haben.
{{< /newsy-block >}}

---

## Was ist ein TYPO3camp?
Das "TYPO3camp" ist ein zweitägiger Anlass, welcher im Barcamp-Stil durchgeführt wird. Oft werden natürlich
Themen bearbeitet, welche irgendwas mit [TYPO3](//typo3.org) zu tun haben, aber das Camp ist komplett offen, um Themen
aufzunehmen, die unabhängig von TYPO3 auf Interesse stossen. [Wie das genau gehen soll und weiteres
zur Struktur des Camps findest du hier](/format).
